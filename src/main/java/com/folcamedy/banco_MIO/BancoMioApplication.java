package com.folcamedy.banco_MIO;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BancoMioApplication {

	public static void main(String[] args) {
		SpringApplication.run(BancoMioApplication.class, args);
	}

}
